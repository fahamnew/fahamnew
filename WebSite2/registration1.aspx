﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="registration1.aspx.cs" Inherits="registration1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Faham-signup
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link href="style.css" rel="stylesheet" />
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">

    <div class="form">
          <div>   
          <h1>Sign Up for Free</h1>         
          <div class="field-wrap">
                <asp:Label runat="server" ForeColor="#939393" Text="Name"></asp:Label>
            <input type="text" runat="server" autocomplete="on" id="nam"/>
          </div>          
                       
          <div class="field-wrap">
                <asp:Label runat="server" ForeColor="#939393" Text="Email"></asp:Label>
            <input type="email" runat="server" autocomplete="on"  id="emailt" />
          </div>   
               <div class="field-wrap">
                <asp:Label runat="server" ForeColor="#939393" Text="Mobile Number"></asp:Label>
            <input type="number" runat="server" autocomplete="on"  id="number"/>
          </div> 
              <div class="field-wrap">
                <asp:Label runat="server" ForeColor="#939393" Text="Password"></asp:Label>
            <input type="password" runat="server" autocomplete="on"  id="password"/>
          </div>          
               <p class="forgot"><a href="log.aspx" >Sign In ?</a></p>
       <asp:Button ID="signup" runat="server" CssClass="button button-block" Text="Sign Up" OnClick="signup_Click" Height="63px" style="margin-top: 9px" />
               
              
      </div>                
      </div>
   

</asp:Content>

