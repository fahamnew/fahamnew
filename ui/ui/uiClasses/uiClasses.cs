﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ui.UiClasses
{
    class UiClasses
    {
        public Double Device_id { get; set; }
        public Double Reading { get; set; }

        static string myconnstrng = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        //updating the database
        public bool Update(UiClasses c)
        {
            bool issuccss = false;
            //connect database
            SqlConnection conn = new SqlConnection(myconnstrng);
            try
            {
                //query to update the water meter value
                string sql = "UPDATE faham SET value=@Reading WHERE dev_id=@Device_id";

                //creating sql cmd
                SqlCommand cmd = new SqlCommand(sql, conn);

                //creatingparameters to add
                cmd.Parameters.AddWithValue("@Reading", c.Reading);
                cmd.Parameters.AddWithValue("@Device_id", c.Device_id);

                //open connection
                conn.Open();

                int rows = cmd.ExecuteNonQuery();
                if(rows>0)
                {
                    issuccss = true;
                }
                else
                {
                    issuccss = false;
                }

            }catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            //return
            return issuccss;
        }
    }
}
