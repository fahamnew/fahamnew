﻿namespace ui
{
    partial class watr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(watr));
            this.Dev = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SUBMIT = new System.Windows.Forms.Button();
            this.Device = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Read = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Dev
            // 
            this.Dev.BackColor = System.Drawing.Color.Transparent;
            this.Dev.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Dev.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dev.ForeColor = System.Drawing.Color.DarkRed;
            this.Dev.Location = new System.Drawing.Point(262, 281);
            this.Dev.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Dev.Name = "Dev";
            this.Dev.Size = new System.Drawing.Size(141, 47);
            this.Dev.TabIndex = 1;
            this.Dev.Text = "DEVICE ID";
            this.Dev.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Dev.Click += new System.EventHandler(this.Dev_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(258, 370);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 47);
            this.label1.TabIndex = 2;
            this.label1.Text = "READING";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SUBMIT
            // 
            this.SUBMIT.BackColor = System.Drawing.Color.SteelBlue;
            this.SUBMIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SUBMIT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SUBMIT.Location = new System.Drawing.Point(411, 434);
            this.SUBMIT.Margin = new System.Windows.Forms.Padding(4);
            this.SUBMIT.Name = "SUBMIT";
            this.SUBMIT.Size = new System.Drawing.Size(181, 64);
            this.SUBMIT.TabIndex = 4;
            this.SUBMIT.Text = "SUBMIT";
            this.SUBMIT.UseVisualStyleBackColor = false;
            this.SUBMIT.Click += new System.EventHandler(this.SUBMIT_Click);
            // 
            // Device
            // 
            this.Device.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Device.Cursor = System.Windows.Forms.Cursors.Default;
            this.Device.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Device.Location = new System.Drawing.Point(411, 283);
            this.Device.Margin = new System.Windows.Forms.Padding(4);
            this.Device.MaxLength = 18;
            this.Device.Name = "Device";
            this.Device.Size = new System.Drawing.Size(299, 45);
            this.Device.TabIndex = 5;
            this.Device.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Device.TextChanged += new System.EventHandler(this.Device_TextChanged);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(1080, 13);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 37);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Read
            // 
            this.Read.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Read.Cursor = System.Windows.Forms.Cursors.Default;
            this.Read.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Read.Location = new System.Drawing.Point(411, 372);
            this.Read.Margin = new System.Windows.Forms.Padding(4);
            this.Read.MaxLength = 18;
            this.Read.Name = "Read";
            this.Read.Size = new System.Drawing.Size(299, 45);
            this.Read.TabIndex = 7;
            this.Read.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(-3, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1129, 54);
            this.panel1.TabIndex = 8;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Aquamarine;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(145, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(842, 44);
            this.label2.TabIndex = 9;
            this.label2.Text = "R AND S APARTMENT WATER METER READING";
            this.label2.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Location = new System.Drawing.Point(-3, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 569);
            this.panel2.TabIndex = 10;
            // 
            // watr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1122, 615);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Read);
            this.Controls.Add(this.Device);
            this.Controls.Add(this.SUBMIT);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dev);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "watr";
            this.Text = "watr";
            this.TransparencyKey = System.Drawing.Color.Gray;
            this.Load += new System.EventHandler(this.watr_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.watr_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.watr_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.watr_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Dev;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SUBMIT;
        private System.Windows.Forms.TextBox Device;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Read;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
    }
}

