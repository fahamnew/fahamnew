﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ui.UiClasses;

namespace ui
{
    public partial class watr : Form
    {
        bool flag = false;
        public int xx, yy;
        public watr()
        {
            InitializeComponent();
        }
        UiClasses c = new UiClasses();
        private void SUBMIT_Click(object sender, EventArgs e)
        {

            if(Device.Text!="" && Read.Text!="")
            { 
            c.Device_id = Convert.ToDouble(Device.Text);
            c.Reading = Convert.ToDouble(Read.Text);
                //insert to database using method
                bool success = c.Update(c);
                if (success == true)
                {
                    MessageBox.Show("Water meter value updated");
                }
                else
                {
                    MessageBox.Show("Not able to update");
                }
            }
            else
            {
                MessageBox.Show("Check the entered value");
            }
        }
        private void watr_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void watr_MouseUp(object sender, MouseEventArgs e)
        {

            flag = false;
        }

        private void watr_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                flag = true;
                xx = e.X;
                yy = e.Y;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void Dev_Click(object sender, EventArgs e)
        {

        }

        private void Device_TextChanged(object sender, EventArgs e)
        {

        }

        private void watr_MouseMove(object sender, MouseEventArgs e)
        {
            if (flag == true)

            {
                Point tmp = new Point();
                tmp.X = this.Location.X+(e.X-xx);
              
                tmp.Y =this.Location.Y+(e.Y-yy);
                this.Location = tmp;
               

            }
        }
    }

       
            
}
